from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from service import config
from service.models import Base

engine = create_async_engine(config.DB_URL, echo=False)
async_session_maker = async_sessionmaker(engine, expire_on_commit=False)


async def init():
    """Create tables in database."""

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all, checkfirst=True)


async def drop():
    """Drop tables in database."""

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
