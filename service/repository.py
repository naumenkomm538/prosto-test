from service.database import async_session_maker
from service.models import User


class UserRepository:

    @staticmethod
    async def create_user(user: User) -> User:
        user = User(
            name=user.name,
            email=user.email,
            hashed_password=user.hashed_password,
        )
        async with async_session_maker() as session:
            async with session.begin():
                session.add(user)
            await session.refresh(user)
        return user

    @staticmethod
    async def get_user(user_id: int) -> User:
        async with async_session_maker() as session:
            user = await session.get(User, user_id)
            return user
