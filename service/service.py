from service.models import User
from service.repository import UserRepository
from service.schemas import UserDTO


class UserService:
    def __init__(self):
        self.repository = UserRepository()

    async def create_user(self, user: UserDTO) -> UserDTO:
        user = User(
            name=user.name,
            email=user.email,
            hashed_password=user.hashed_password,
        )
        user = await self.repository.create_user(user)
        return UserDTO.model_validate(user, from_attributes=True)

    async def get_user(self, user_id: int) -> UserDTO:
        user = await self.repository.get_user(user_id)
        return UserDTO.model_validate(user, from_attributes=True)
