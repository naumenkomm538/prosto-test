from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy.orm import DeclarativeBase, declared_attr, Mapped, mapped_column


class Base(DeclarativeBase):
    @declared_attr
    def __tablename__(cls) -> str:  # noqa
        return cls.__name__.lower()


class User(Base):
    __table_args__ = (PrimaryKeyConstraint("id"),)

    id: Mapped[int]
    email: Mapped[str]
    hashed_password: Mapped[str]
    name: Mapped[str] = mapped_column(nullable=True)
