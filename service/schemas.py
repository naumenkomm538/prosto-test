from pydantic import BaseModel


class UserDTO(BaseModel):
    id: int | None = None
    name: str | None = None
    email: str
    hashed_password: str
