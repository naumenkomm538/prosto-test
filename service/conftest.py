import pytest

from service.database import init, drop
from service.models import User
from service.repository import UserRepository
from service.schemas import UserDTO
from service.service import UserService


@pytest.fixture(name="database")
async def get_db():
    await init()
    yield
    await drop()


@pytest.fixture(name="user_repository")
def get_user_repository():
    return UserRepository()


@pytest.fixture(name="user_service")
def get_user_service():
    return UserService()


@pytest.fixture(name="user_db")
async def get_user_db(user_repository):
    user = User(name="John Doe1", email="j.d1@gmail.com", hashed_password="12341")
    user_db = await user_repository.create_user(user)
    return user_db


@pytest.fixture(name="user_dto")
def get_user_dto():
    return UserDTO(name="John Doe2", email="j.d2@gmail.com", hashed_password="12342")
