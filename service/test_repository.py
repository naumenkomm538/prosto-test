import pytest


@pytest.mark.asyncio
async def test_create_user(database, user_db, user_repository):
    user2 = await user_repository.create_user(user_db)
    assert user_db.id != user2.id
    assert user_db.name == user2.name
    assert user_db.email == user2.email
    assert user_db.hashed_password == user2.hashed_password


@pytest.mark.asyncio
async def test_get_user(database, user_db, user_repository):
    user = await user_repository.get_user(user_db.id)
    assert user_db.id == user.id
