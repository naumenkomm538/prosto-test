import pytest


@pytest.mark.asyncio
async def test_create_user(database, user_dto, user_service):
    user = await user_service.create_user(user_dto)
    assert user.id is not None
    assert user.name == user_dto.name
    assert user.email == user_dto.email
    assert user.hashed_password == user_dto.hashed_password


@pytest.mark.asyncio
async def test_get_user(database, user_db, user_dto, user_service):
    user = await user_service.get_user(user_db.id)
    assert user_db.id == user.id
    assert user_db.name == user.name
    assert user_db.email == user.email
    assert user_db.hashed_password == user.hashed_password
