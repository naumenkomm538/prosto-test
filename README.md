# Task 1. Hashmap.

---

### Notes
    task 1:
    Implement own hashmap class (put, get methods are required).
    write tests for this class.
    add notes with argumentation for choosen implementation.

### Argumentation
Implement own hashmap class:
- [x] put method
- [x] get method
- [x] write tests
- [x] add notes with argumentation for chosen implementation


### Commands
```bash
# Run pytest Hashmap tests
pytest hashmap -v
```

---

# Task 2. Service class.

#### Notes
    task 2:

    Write small service class with methods
    get(user_id) → UserDTO
    add(user: UserDTO)
    
    signatures can be changed if you think it's needed for better implementation
    
    UserDTO - pydantic model
    users are stored in db.
    assume you have method get_async_session, which returns AsyncSession sqlalchemy object to interact with db
    
    write tests for that class
    
    add notes with argumentation for choosen implementation

### Structure
- [config.py](service%2Fconfig.py) - project configuration (environment variables)
- [conftest.py](service%2Fconftest.py) - pytest fixtures
- [database.py](service%2Fdatabase.py) - database connection and db utils
- [models.py](service%2Fmodels.py) - database models
- [repository.py](service%2Frepository.py) - repository class for user operations (database operations)
- [schemas.py](service%2Fschemas.py) - pydantic models
- [service.py](service%2Fservice.py) - service class for user operations (business logic)
- [test_repository.py](service%2Ftest_repository.py) - tests for repository class
- [test_service.py](service%2Ftest_service.py) - tests for service class


### Commands
```bash
# Run pytest Service tests
pytest service -v
```


---
# Tech stack
- Python 3.11
- Poetry
- SQLAlchemy
- AsyncSQLite
- Pydantic
- Pytest asyncio
- Ruff