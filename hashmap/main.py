class Node:
    """
    Node class to store key-value pairs in the linked list.
    """

    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        """Pointer to the next node in the linked list"""


class HashMap:
    def __init__(self):
        self.size = 6
        """Initial size of the hash table."""

        self.map = [None] * self.size
        """Array to store key-value pairs."""

        self.threshold = 0.7
        """Load factor threshold for resizing"""

    def _get_hash(self, key, size=None):
        """
        Customizable hash function
        """
        return hash(key) % (size or self.size)  # Use built-in hash function for simplicity

    def put(self, key, value):
        """
        Inserts a key-value pair into the hash map, handling collisions using separate chaining.
        """
        bucket_index = self._get_hash(key)

        # Check for collision (same bucket index for another key)
        if self.map[bucket_index] is not None:
            # Collision handling using separate chaining: Create a linked list (Node) if not already present
            head = self.map[bucket_index]
            while head is not None:
                if head.key == key:
                    # Update existing value if key already exists
                    head.value = value
                    return
                head = head.next

            # If key not found (new insertion): Create a new Node and prepend it to the linked list
            new_node = Node(key, value)
            new_node.next = self.map[bucket_index]
            self.map[bucket_index] = new_node

        else:
            # No collision: Directly insert into the bucket
            self.map[bucket_index] = Node(key, value)

        # Check load factor and resize if necessary
        self._check_and_resize()

    def get(self, key):
        """
        Retrieves the value associated with a key, handling collisions.
        """
        bucket_index = self._get_hash(key)
        head = self.map[bucket_index]

        while head is not None:
            if head.key == key:
                return head.value
            head = head.next

        return None  # Key not found

    def remove(self, key):
        """
        Removes the key-value pair from the hash map, handling collisions.
        """
        bucket_index = self._get_hash(key)
        head = self.map[bucket_index]
        prev = None

        while head is not None:
            if head.key == key:
                if prev is None:
                    self.map[bucket_index] = head.next  # Remove head of the linked list
                else:
                    prev.next = head.next  # Remove the node from the linked list
                return
            prev = head
            head = head.next

        return None  # Key not found

    def _check_and_resize(self):
        """
        Checks the load factor and resizes the hash table if necessary to maintain efficiency.
        """
        load_factor = len(self.get_all_key_value_pairs()) / self.size  # Calculate load factor
        if load_factor > self.threshold:
            # Resize: Create a new hash table with double the size and rehash all elements
            new_size = self.size * 2
            new_map = [None] * new_size
            for bucket in self.map:
                head = bucket
                while head is not None:
                    new_bucket_index = self._get_hash(head.key, new_size)  # Rehash with new size
                    new_node = Node(head.key, head.value)
                    # Insert into new table using separate chaining
                    if new_map[new_bucket_index] is not None:
                        new_node.next = new_map[new_bucket_index]
                    new_map[new_bucket_index] = new_node
                    head = head.next
            self.size = new_size
            self.map = new_map

    def get_all_key_value_pairs(self):
        """
        Retrieves all key-value pairs as a list of tuples.
        """
        all_pairs = []
        for bucket in self.map:
            head = bucket
            while head is not None:
                all_pairs.append((head.key, head.value))  # Append key-value tuple
                head = head.next
        return all_pairs
