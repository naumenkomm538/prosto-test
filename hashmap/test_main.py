import pytest

from hashmap.main import HashMap


@pytest.fixture
def hashmap():
    return HashMap()


def test_put(hashmap):
    hashmap.put(1, "one")
    assert hashmap.get(1) == "one"


def test_get(hashmap):
    hashmap.put(1, "one")
    assert hashmap.get(1) == "one"
    assert hashmap.get(2) is None


def test_remove(hashmap):
    hashmap.put(1, "one")
    hashmap.remove(1)
    assert hashmap.get(1) is None


def test_collision(hashmap):
    hashmap.put(1, "one")
    hashmap.put(7, "seven")
    assert hashmap.get(1) == "one"
    assert hashmap.get(7) == "seven"
    hashmap.remove(1)
    assert hashmap.get(1) is None
    assert hashmap.get(7) == "seven"
    hashmap.remove(7)
    assert hashmap.get(1) is None
    assert hashmap.get(7) is None
